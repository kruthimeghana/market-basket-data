++++++++++++++++
Market Basket Data
++++++++++++++++

**Description:**
Given a Market Basket Data as an input file we have to form all the association rules based on the **threshold computation method**.


**Tools:** ::

	1. python.
	2. Jupyter.
